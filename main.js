// Modules to control application life and create native browser window
const { app, BrowserWindow, globalShortcut } = require("electron");
const path = require("path");

function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 900,
    height: 600,
    minHeight: 400,
    minWidth: 600,
    show: false
    /* frame: false,
    /* titleBarStyle: 'hidden' */
  });
  // and load the index.html of the app.
  // mainWindow.loadURL(url.format({
  //   pathname: path.join(__dirname, 'index.html'),
  //   protocol: 'file:',
  //   slashes: true

  mainWindow.once("ready-to-show", () => {
    mainWindow.show();
  });
  // }))
  // and load the url of the app.
  mainWindow.loadURL("https://polytimer.rocks/");
  // Open the DevTools.
  // mainWindow.webContents.openDevTools()
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", createWindow);

// Quit when all windows are closed.
app.on("window-all-closed", function() {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") app.quit();
});

app.on("activate", function() {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow();
});

// when app is ready register a global shortcut
// that calls createWindow function
app.on("ready", () => {
  const shortcut = globalShortcut.register("CommandOrControl+K", () => {
    mainWindow.show();
  });
});

// do not quit when all windows are closed
// and continue running on background to listen
// for shortcuts
app.on("window-all-closed", e => {
  e.preventDefault();
  e.returnValue = false;
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
